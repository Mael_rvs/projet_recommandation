import pandas as pd
import random
from scipy.stats import pearsonr
import copy

"""déclaration des variables et lecture des fichiers csv"""
df = pd.read_csv("toy_incomplet.csv", sep="\t")
df2 = pd.read_csv("toy_complet.csv", sep="\t")
df3 = copy.copy(df)

"""calcul de la similarité pearson entre utilisateur"""
def sim_pearsonU(i, k):
    vec_a = []
    vec_b = []
    cos_sim = -1.0
    for j in range(1, 1001):
        if df.iloc[i, j] != -1 and df.iloc[k, j] != -1:
            vec_a.append(df.iloc[i, j])
            vec_b.append(df.iloc[k, j])
    sim_pearson, p_value = pearsonr(vec_a,vec_b)
    vec_a.clear()
    vec_b.clear()
    return sim_pearson

"""calcul de la pearson entre items"""
def sim_pearsonI(j, k):
    vec_a = []
    vec_b = []
    cos_sim = -1.0
    for i in range(1, 100):
        if df.iloc[i, j] != -1 and df.iloc[i, k+1] != -1:
            vec_a.append(df.iloc[i, j])
            vec_b.append(df.iloc[i, k+1])
    sim_pearson, p_value  = pearsonr(vec_a,vec_b)
    vec_a.clear()
    vec_b.clear()
    return sim_pearson

"""calcul de la similarité cosinus entre utilisateur"""
def sim_cosU(i, k):
    vec_a = []
    vec_b = []
    cos_sim = -1.0
    for j in range(1, 1001):
        if df.iloc[i, j] != -1 and df.iloc[k, j] != -1:
            vec_a.append(df.iloc[i, j])
            vec_b.append(df.iloc[k, j])
    H = sum(a * b for a, b in zip(vec_a, vec_b))
    s_a = sum(a * a for a in vec_a) ** 0.5
    s_b = sum(b * b for b in vec_b) ** 0.5
    cos_sim = H / (s_a * s_b)
    vec_a.clear()
    vec_b.clear()
    return cos_sim
     
"""calcul de la similarité entre items"""
def sim_cosI(j, k):
    vec_a = []
    vec_b = []
    cos_sim = -1.0
    for i in range(1, 100):
        if df.iloc[i, j] != -1 and df.iloc[i, k+1] != -1:
            vec_a.append(df.iloc[i, j])
            vec_b.append(df.iloc[i, k+1])
    H = sum(a * b for a, b in zip(vec_a, vec_b))
    s_a = sum(a * a for a in vec_a) ** 0.5
    s_b = sum(b * b for b in vec_b) ** 0.5
    cos_sim = H / (s_a * s_b)
    vec_a.clear()
    vec_b.clear()
    return cos_sim

"""note de l'utilisateur k"""
def note(k ,j):
    return df.iloc[k,j]

"""calcul de la moyenne des notes des utilisateurs"""
def moyenneU(i):
    result = 0
    nb = 0
    for j in range(1 ,1000):
        if df.iloc[i, j] != -1:
            result += df.iloc[i, j]
            nb = nb + 1
    return result/nb

"""calcul de la moyenne des notes d'items"""
def moyenneI(j):
    result = 0
    nb = 0
    for i in range(0 ,100):
        if df.iloc[i, j] != -1:
            result += df.iloc[i, j]
            nb = nb + 1
    return result/nb

"""Le programme principal"""
nbutilisateur = 100
listepred = []
listepredRandom = []
for i in range(0,100):
    for j in range(0,1001):
        df3.iloc[i, j] = copy.copy(df.iloc[i, j])
        if df.iloc[i, j] == -1:
            a = 0
            b = 0
            for k in range(0, nbutilisateur):

                if i != k and note(k, j) != -1 :

                    """Similarité cosinus"""
                    a += sim_cosU(i, k) * (note(k, j) - moyenneI(j))
                    b += sim_cosU(i, k)
                    
                    """ai += sim_cosI(j, k) * (note(k, j) - moyenneU(i))"""
                    """bi += sim_cosI(j, k)"""

            """agrégation de note"""
            """noteTI = round(moyenneI(j) + (ai/bi))"""
            noteTU = moyenneU(i) + (a/b)
            print("La valeur trouvée")
            print(noteTU)
            print("L'erreur")
            print(noteTU - df2.iloc[i,j])
            df3.iloc[i,j] = noteTU
            listepred.append(noteTU - df2.iloc[i,j])
            listepredRandom.append(random.randint(0,5) - df2.iloc[i,j])

"""L'affichage"""

print("Le biais est de : ")
erreurB = (sum(a for a in listepred))/len(listepred)
print(erreurB)

print("Le biais randomisé est de : ")
erreurBR = (sum(a for a in listepredRandom))/len(listepredRandom)
print(erreurBR)

print("L'erreur Moyenne est de : ")
erreurM = (sum((a * a)**0.5 for a in listepred))/len(listepred)
print(erreurM)

print("L'erreur Moyenne randomisé est de : ")
erreurMR = (sum((a * a)**0.5 for a in listepredRandom))/len(listepredRandom)
print(erreurMR)

df3.to_csv('myFile.csv', sep = '\t')
print("finit")